  /***********************************
  *              2015                *
  *    Alexandr Loginov-Solonitsyn   *
  *       login2030@gmail.com        *
  ***********************************/
  (function($){
    jQuery.fn.laoPopup = function(options, func){
      options = $.extend({
        content: '<p>Content</p>',
        close: 'Close',
        view: 'classic'
      }, options);

      var make = function(){
        // Кнопка
        var that = $(this),
            id = that.context.id, // Равнозначно that.attr('id')
            firstDiv = $('div:first'), // Первый див на странице (обертка)
            intScrolled = $(window).scrollTop(), // Сколько пикселов проскроллили
            popup,
            wrapper;
        
        if ($('.popup_wrap_' + id).length == 0) {          
          if(options.view == 'full') {
            // Если задан параметр full
            markingPoupClassicAndFull();
            wrapper = popup.parent();
           } else {
            // Разметка по дефолту
            markingPoupClassicAndFull();
            popup.wrap('<div class="table"></div>');
            popup.wrap('<div class="table_cell"></div>');
            wrapper = popup.parent().parent().parent();
          }
        }
        // Обрабатываем клик на кнопку вызова окна
        that.click(function(){
          var popupContent;
        // Дополнительный див здесь для того, чтобы возможно было создавать кнопку закрытия окна
        if ($('.popup_content_' + id).length == 0) {
          popup.append('<div class="popup_content_' + id + '"></div>');
        }
        
        popupContent = popup.find('.popup_content_' + id);
        popupContent.append(options.content);
        wrapper.fadeIn();
        for (;;) {
          if (func) {
            func()
          } else {
            break;
          }
          break;
        }

        });
        
        closeElem.click(function(e){
          var popupContent = popup.find('.popup_content_' + id);
          wrapper.fadeOut(500);

          setTimeout(function() {
            popupContent.html('');
          }, 400);
        });
        /* functions */
        function markingPoupClassicAndFull() {
          that.after('<div class="popup_' + id + '"></div>');
          popup = $('.popup_' + id);
          popup.append('<div class="close_' + id + '">' + options.close + '</div>');
          closeElem = popup.find('.close_' + id);
          popup.wrap('<div class="popup_wrap_' + id + '"></div>');
        }
      };
      
      return this.each(make);
    };
  })(jQuery);