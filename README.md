# laoPopup

jQuery popup window plugin.You can use html, text, images for content.

## Usage
The plug must be done with any button a unique id.
Plug accepts three parameters:

```
#!html
$('#id').laoPopup({
  content: 'contents for popup'
  close: 'element close popup(any html or images)'
},
function() {
/* optional anonymous function */
/* is called when you click on the call button popup */
}
)
```

Below is just an example.

## Example
HTML code
```
#!html

<button id="cause_popup">Cause popup</button>
```
Javascript code
```
#!javascript
$('#cause_popup').laoPopup({
  content: '<p>My html content</p>',
  close: '<button>close</button>'
}, function() {
  console.log('popup');
});
```
## Result HTML
```
#!html
<button id="demo">Call popup</button>
<div class="popup_wrap_demo" style="display: none;">
  <div class="table">
    <div class="table_cell">
      <div class="popup_demo">
        <div class="close_demo">
          ...close...
        </div>
        <div class="popup_content_demo">
          ...content...
        </div>
      </div>
    </div>
  </div>
</div>
```

## Demo
[http://novostiva.ru/laoPopup/](http://novostiva.ru/laoPopup/)